0 Создан собственный репозиторий на GitHub
```
https://bitbucket.org/repo/create

Workspace: Ilya Vasilyev
Project: Untitled project
Repository name: git-basic-test
Access level
Include a README? No
Default branch name: master
Include .gitignore? No
```

1 Клонировать на свой локальный компьютер пустой репозиторий
```
git clone https://ilya_vasilyev@bitbucket.org/ilya_vasilyev/git-basic-test.git
cd git-basic-test
```

2 Добавить в свой локальный репозиторий (прямо в master) файл Potter.txt, содержащий список книг (именно в том виде, как оно приведено ниже):

    Harry Potter book
    One The Philosopher's Stone (1997)
    two The Chamber of Secrets (1998)
    tri The Prisoner of Azkaban (1999)
    4etyre The Goblet of Fire (2000)
    Five Order of the Phoenix (2003)
    Six Half-Blood Prince (2005)
    Seven Deathly Hallows (2007)

    После добавить(push) файл Potter.txt в свой удалённый репозиторий на GitHub

```
git config --global user.name "Ilya Vasilyev"

git config --global user.email "ilya_vasilyev@bitbucket.org"

git config --global --list

cat > "Potter.txt" <<EOF
Harry Potter book
One The Philosopher's Stone (1997)
two The Chamber of Secrets (1998)
tri The Prisoner of Azkaban (1999)
4etyre The Goblet of Fire (2000)
Five Order of the Phoenix (2003)
Six Half-Blood Prince (2005)
Seven Deathly Hallows (2007)
EOF

git add "Potter.txt"

git commit -m "Commit 1"

git push
```

3 Создать ветку feature, изменить в ней содержание файла Potter.txt на новое:

    Harry Potter book
    One The Philosopher's Stone (1997)
    Two The Chamber of Secrets (1998)
    Three The Prisoner of Azkaban (1999)
    chetyre The Goblet of Fire (2000)
    Five Order of the Phoenix (2003)
    Six Half-Blood Prince (2005)
    Seven Deathly Hallows (2007)

    После этого закоммитить (commit) изменения в ветку feature
    
```
git branch feature

git checkout feature

echo "Harry Potter book
One The Philosopher's Stone (1997)
Two The Chamber of Secrets (1998)
Three The Prisoner of Azkaban (1999)
chetyre The Goblet of Fire (2000)
Five Order of the Phoenix (2003)
Six Half-Blood Prince (2005)
Seven Deathly Hallows (2007)
" > "Potter.txt"

git commit -am "Commit 2"
```

4 Через master, изменить содержание файла Potter.txt на ещё один вариант:

    Harry Potter book
    One The Philosopher's Stone (1997)
    Two The Chamber of Secrets (1998)
    tri The Prisoner of Azkaban (1999)
    Four The Goblet of Fire (2000)
    Five Order of the Phoenix (2003)
    Six Half-Blood Prince (2005)
    Seven Deathly Hallows (2007)

    После закоммитить (commit) изменения в ветку master и 
    сделать push в удалённый репозиторий

```
git checkout master

echo "Harry Potter book
One The Philosopher's Stone (1997)
Two The Chamber of Secrets (1998)
tri The Prisoner of Azkaban (1999)
Four The Goblet of Fire (2000)
Five Order of the Phoenix (2003)
Six Half-Blood Prince (2005)
Seven Deathly Hallows (2007)" > "Potter.txt"

git commit -am "Commit 3"

git push --all origin
```

5 Снова перейти в ветку feature и вмерджить в неё master...
```
git checkout feature

git merge master  # Automatic merge failed...
```

6 ...таким образом, чтобы в ветке feature появился правильный вариант Potter.txt:

    Harry Potter book
    One The Philosopher's Stone (1997)
    Two The Chamber of Secrets (1998)
    Three The Prisoner of Azkaban (1999)
    Four The Goblet of Fire (2000)
    Five Order of the Phoenix (2003)
    Six Half-Blood Prince (2005)
    Seven Deathly Hallows (2007)

    После этого закоммитить (commit) изменения ветки feature

```
git mergetool

# :wqa да, это vi >_<

git commit -am "Commit 4"
```

7 Изменить файл в ветке feature добавив в него «:» в конце самой 1-ой строки:

    Harry Potter book:
    One The Philosopher's Stone (1997)
    Two The Chamber of Secrets (1998)
    Three The Prisoner of Azkaban (1999)
    Four The Goblet of Fire (2000)
    Five Order of the Phoenix (2003)
    Six Half-Blood Prince (2005)
    Seven Deathly Hallows (2007)

    После этого закоммитить (commit) изменения ветки feature

```
git checkout feature

echo "Harry Potter book:
One The Philosopher's Stone (1997)
Two The Chamber of Secrets (1998)
Three The Prisoner of Azkaban (1999)
Four The Goblet of Fire (2000)
Five Order of the Phoenix (2003)
Six Half-Blood Prince (2005)
Seven Deathly Hallows (2007)
" > "Potter.txt"

git commit -am "Commit 5"
```

8 Через master изменить содержание файла Potter.txt на ещё один вариант:

    Harry Potter book
    One The Philosopher's Stone (1997)
    Two The Chamber of Secrets (1998)
    tri The Prisoner of Azkaban (1999)
    4 The Goblet of Fire (2000)
    Five Order of the Phoenix (2003)
    Six Half-Blood Prince (2005)
    Seven Deathly Hallows (2007)

    После этого закоммитить (commit) изменения ветки master

```
git checkout master

echo "Harry Potter book
One The Philosopher's Stone (1997)
Two The Chamber of Secrets (1998)
tri The Prisoner of Azkaban (1999)
4 The Goblet of Fire (2000)
Five Order of the Phoenix (2003)
Six Half-Blood Prince (2005)
Seven Deathly Hallows (2007)
" > "Potter.txt"

git commit -am "Commit 6"
```

9 Вмерджить feature в master и обновить Potter.txt, разрешить конфликт в ветке master так, чтобы в результате получился правильный вариант текста:

    Harry Potter book:
    One The Philosopher's Stone (1997)
    Two The Chamber of Secrets (1998)
    Three The Prisoner of Azkaban (1999)
    Four The Goblet of Fire (2000)
    Five Order of the Phoenix (2003)
    Six Half-Blood Prince (2005)
    Seven Deathly Hallows (2007)

    Залить(push) изменения ветки master в удалённый репозиторий

```
git merge feature  # Automatic merge failed...

git mergetool

git commit -am "Commit 7"

git push origin master
```
