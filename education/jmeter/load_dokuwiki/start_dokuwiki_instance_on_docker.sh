#!/usr/bin/env bash

# To be executed manually.

export NETWORK="dokuwiki-network"
# For the first time, run:
# docker network create "${NETWORK}"

# Based on `/opt/bitnami/apache/conf/httpd.conf`, the server listens on the 8080 port
export IMG="bitnami/dokuwiki:latest" && \
docker pull "${IMG}" && \
docker run \
    --env DOKUWIKI_PASSWORD=my_password \
    --interactive \
    --network "${NETWORK}" \
    --publish 80:8080/tcp \
    --publish 443:8443/tcp \
    --rm \
    --tty \
    "${IMG}" bash

apachectl start
# Now the web server must be accessible from the URL: `http://<server_ip>/`

# Setup page: http://<server_ip>/install.php
# Enable ACL
# Initial ACL policy: Public Wiki
# Allow users to register themselves 

# Enable pasword auth
sed -i "s/^\$conf\['autopasswd'\].*/\$conf\['autopasswd'\] = 0;/g" "/opt/bitnami/dokuwiki/conf/dokuwiki.php"
grep 'autopasswd' "/opt/bitnami/dokuwiki/conf/dokuwiki.php"
