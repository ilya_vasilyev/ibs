#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import random
import string
import numpy as np
import pandas as pd

CHARS = 6
USERS = 10 ** 6
PREFIX = "testuser"

csv_file = "D:\\data\\generated_user_data_set.csv"
# mklink /J "D:\\data" "..."

values = [{
    "userid": f"{PREFIX}{str(i + 1).zfill(np.ceil(np.log10(USERS)).astype(int) + 1)}",
    "email": "{}_{}@mail.com".format(
        PREFIX, 
        "".join(
            random.choices(string.ascii_lowercase + string.digits, k = CHARS)
        )
    ),
    "password": "password1"
} for i in range(USERS)]

data = pd.DataFrame(values)
data.to_csv(csv_file, encoding="utf-8", sep=",", index=False, header=False)
